# PlainsenseGateway

Open Source Release of the Plainsense Gateway PCB. Contains Kicad project files including schematics and design and converted schematics pdf. Enjoy!

For more Information check out our [prouct page](https://plaincore.de/products/plainsense/)

![Schematics Example](./img/sch1.png)
![Schematics Example](./img/sch.png)

![Design Example](./img/top.png)
